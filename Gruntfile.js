module.exports = function(grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		browserSync: {
			bsFiles: {
                src: [
                    'css/*.css',
                    'js/*.js',
                    '*.html'
                ]
            },
			options: {
				watchTask: true,
	            proxy: "localhost"
	        }
		},
		watch: {
			styles: {
				files: ['scss/**/*'],
				tasks: ['sass', 'postcss'],
				options: {
				   spawn: false
				}
			},
			scripts: {
				files: ['js/**/*.js'],
				tasks: ['browserify', 'uglify'],
				options: {
				  spawn: false
				}
			}
		},
		sass: {
			options: {
				sourceMap: true,
				sourceMapFilename: 'css/site.min.css.map',
                sourceMapURL: 'site.min.css.map',
				outputStyle: 'compressed'
			},
			dist: {
				files: {
					'css/site.min.css': 'scss/base.scss'
				}
			}
		},
		postcss: {
			options: {
				map: true,
				processors: [
				require('autoprefixer')({browsers: ['last 5 versions']})
				]
			},
			dist: {
				files: {
					'css/site.min.css': 'css/site.min.css'
				}
			}
		},		
		browserify: {
			dist: {
				options: {
					browserifyOptions: {
						debug: true
					},
					transform: [
						["babelify"]
					]
				},
				files: {
				   "js/site.min.js": ["js/site.js"]
				}
			}
		},
		uglify: {
			dist: {
				files: {
					'js/site.min.js': ['js/site.min.js']
				}
			}
		}

	});

	grunt.registerTask("default", "Prints usage", function () {
		grunt.log.writeln("");
		grunt.log.writeln("Setting up");
		grunt.log.writeln("------------------------");
		grunt.log.writeln("");
		grunt.log.writeln("* run 'grunt --help' to get an overview of all commands.");
		grunt.log.writeln("* run 'grunt dev' to start watching and compiling SASS and JS changes for development.");
	});

	grunt.registerTask('dev', ['sass', 'postcss', 'browserSync', 'watch']);
	grunt.registerTask('prod', ['sass', 'postcss', 'browserify', 'uglify']);
}